import pandas as pd
import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import OrdinalEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.compose import ColumnTransformer
from imblearn.over_sampling import SMOTE

categorical_columns = [
    'workclass',
    'education',
    'marital-status',
    'occupation',
    'relationship',
    'race',
    'sex',
    'native-country'
]

numerical_columns = [
    'age',
    'fnlwgt',
    'education-num',
    'capital-gain',
    'capital-loss',
    'hours-per-week'
]

feature_columns = np.concatenate([numerical_columns, categorical_columns])
target_column = "class"


def load_data(file_name: str, skiprows: int = 0) -> pd.DataFrame:
    """Loads the data we want to work with

    Args:
        file_name (str): The name of the file we want to work with.

    Returns:
        pandas.DataFrame: A table containing the information we want to explore.
    """
    columns = [
        "age",
        "workclass",
        "fnlwgt",
        "education",
        "education-num",
        "marital-status",
        "occupation",
        "relationship",
        "race",
        "sex",
        "capital-gain",
        "capital-loss",
        "hours-per-week",
        "native-country",
        "class"
    ]
    return pd.read_csv(file_name, names=columns, skiprows=skiprows, sep=", ", engine="python", na_values="?")


class ReduceCardinality(BaseEstimator, TransformerMixin):
    def __init__(self, reduce=True):
        self.reduce = reduce

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        features_to_reduce_dict = {
            "Never-worked": "Jobless",
            "Without-pay": "Jobless",
            "State-gov": "Gov",
            "Local-gov": "Gov",
            "Preschool": "School",
            "1st-4th": "School",
            "5th-6th": "School",
            "7th-8th": "School",
            "9th": "School",
            "10th": "School",
            "11th": "School",
            "12th": "School",
            "Prof-school": "Doctorate",
            "Assoc-acdm": "Assoc",
            "Assoc-voc": "Assoc",
            "HS-grad": "College",
            "Some-college": "College",
            "Divorced": "No-spouse",
            "Married-spouse-absent": "No-spouse",
            "Separated": "No-spouse",
            "Widowed": "No-spouse",
            "Married-AF-spouse": "No-spouse",
            "Amer-Indian-Eskimo": "Others",
            "Other": "Others",
            "Not-in-family": "Other",
            "Own-child": "Other",
            "Unmarried": "Other",
            "Other-relative": "Other",
            "Cambodia": "Other-country",
            "Canada": "Other-country",
            "China": "Other-country",
            "Columbia": "Other-country",
            "Cuba": "Other-country",
            "Dominican-Republic": "Other-country",
            "Ecuador": "Other-country",
            "El-Salvador": "Other-country",
            "England": "Other-country",
            "France": "Other-country",
            "Germany": "Other-country",
            "Greece": "Other-country",
            "Guatemala": "Other-country",
            "Haiti": "Other-country",
            "Holand-Netherlands": "Other-country",
            "Honduras": "Other-country",
            "Hong": "Other-country",
            "Hungary": "Other-country",
            "India": "Other-country",
            "Iran": "Other-country",
            "Ireland": "Other-country",
            "Italy": "Other-country",
            "Jamaica": "Other-country",
            "Japan": "Other-country",
            "Laos": "Other-country",
            "Mexico": "Other-country",
            "Nicaragua": "Other-country",
            "Outlying-US(Guam-USVI-etc)": "Other-country",
            "Peru": "Other-country",
            "Philippines": "Other-country",
            "Poland": "Other-country",
            "Portugal": "Other-country",
            "Puerto-Rico": "Other-country",
            "Scotland": "Other-country",
            "South": "Other-country",
            "Taiwan": "Other-country",
            "Thailand": "Other-country",
            "Trinadad&Tobago": "Other-country",
            "Vietnam": "Other-country",
            "Yugoslavia": "Other-country"
        }

        for key, value in features_to_reduce_dict.items():
            X[X == key] = value

        return X


def feature_df(df: pd.DataFrame) -> pd.DataFrame:
    return df[feature_columns]


def feature_array(df: pd.DataFrame) -> np.array:
    return df[feature_columns].values


def target_array(df: pd.DataFrame, set: str = 'train') -> np.array:
    if set == 'train':
        return np.where(df[target_column].values == "<=50K", 0, 1)
    elif set == 'test':
        return np.where(df[target_column].values == "<=50K.", 0, 1)
