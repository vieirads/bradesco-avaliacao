from custom_module import *

from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import OrdinalEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.compose import ColumnTransformer
from imblearn.over_sampling import SMOTE
from imblearn.pipeline import Pipeline as imbPipeline
from xgboost import XGBClassifier

from sklearn.metrics import classification_report

main_data = load_data("./data/adult.data")
test_data = load_data("./data/adult.test", skiprows=1)

categorical_preprocessor = Pipeline(steps=[
    ("imputer", SimpleImputer(strategy="most_frequent")),
    ("cardinality", ReduceCardinality()),
    ("encoder", OrdinalEncoder()),
    ("scaler", StandardScaler())
])

numerical_preprocessor = Pipeline(steps=[
    ("scaler", StandardScaler())
])

preprocessor = ColumnTransformer(
    transformers=[
        ("numerical_preprocessor", numerical_preprocessor, numerical_columns),
        ("categorical_preprocessor", categorical_preprocessor, categorical_columns),
    ]
)

model = XGBClassifier(
    colsample_bytree=0.5,
    learning_rate=0.1,
    max_depth=12,
    min_child_weight=1,
    n_estimators=75,
    subsample=1,
    random_state=42
)

model_pipeline = imbPipeline(steps=[
    ("preprocessor", preprocessor),
    ("smote", SMOTE(random_state=42)),
    ("model", model)
])

X_train = feature_df(main_data)
y_train = target_array(main_data)

X_test = feature_df(test_data)
y_test = target_array(test_data, set="test")

model_pipeline.fit(X_train, y_train)
y_pred = model_pipeline.predict(X_test)

print(classification_report(y_test, y_pred, target_names=["<=50K", ">50K"]))
