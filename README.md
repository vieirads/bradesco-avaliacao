![logo_bradesco](./figures/bradesco.png)

# Avaliação - Cientista de Dados

Esse repositório foi desenvolvido com o intuito de mostrar o estudo realizado para **Avaliação de Cientista de Dados do Bradesco**.

## Pacotes necessários

Para que o os códigos possam sem executados é necessário a instalação de algumas bibliotecas como `pandas`, `numpy`, `sklearn`, `xgboost` e `imblearn`.

### Usando pip

Se você usa o [pip](https://pypi.org/)

```bash
pip install pandas
pip install numpy
pip install -U scikit-learn
pip install xgboost
pip install -U imbalanced-learn
```

### Usando conda

Se você usa o [conda](https://docs.conda.io/en/latest/), crie um ambiente usando

```bash
conda create -n nome_do_ambiente python
```

e para ativá-lo

```bash
activate nome_do_ambiente
```

Assim, para instalar as dependências necessárias no projeto use

```bash
conda install pandas
conda install numpy
conda install scikit-learn
conda install py-xgboost
conda install imbalanced-learn
```

## Diretórios e Arquivos

- 📁 `data`: aqui ficam os arquivos usados para fazer as análises exploratórias, preparação dos dados e criação de modelos:

  - 📄 `adult.data`: dados usados para análises exploratórias e treino do modelo;
  - 📄 `adult.test`: dados usados para testar o modelo;
  - 📄 `old.adult.names`: arquivo que contém informações sobre os dados;
  - 📄 `final_train_data.csv`: arquivo com os dados de treino já preprocessados;
  - 📄 `final_test_data.csv`: arquivo com os dados de teste já preprocessados.
    <br><br>

- 📁 `figures`: aqui ficam as figuras geradas durante as análises para serem usadas na aparesentação:

  - 📊 `basic_distributions`: figura mostrando as distribuições básicas dos atributos presentes no dado;
  - 📊 `age_hour_per_week`: figura mostrando a correlação entre idade e horas trabalhadas por semana em destacando a renda;
  - 📊 `correlation_with_income`: correlação dos atributos destacando a variável alvo, isto é, a renda;
  - 📊 `numerical_attr_corr`: correlação entre os atributos numéricos presentes no dado, em que consideramos a renda como `<=50K`= 0 e `>50K`= 1;
  - 📊 `confusion_matrix`: matrix de confusão do modelo final (`XGBoostClassifier`).
    <br><br>

- 📄 `data_analysis.ipynb`: arquivo que contém as análises preliminares, exploratórias para entender melhor o conteúdo dos dados a serem analisados.
- 📄 `data_preparation.ipynb`: arquivo que contém a preparação dos dados para criação do modelo, levando em consideração as observações feitas na análise exploratória.
- 📄 `model_analysis.ipynb`: arquivo que contém o teste de diversos modelos nos dados para que a melhor opção seja escolhida.
- 📄 `final_model.ipynb`: arquivo com a `pipeline` final para execução do modelo.
- 📄 `run_pipeline`: arquivo com toda pipeline para carregar, tratar, fitar os dados e fazer o teste. Esse arquivo tem como output a matrix de confusão do modelo proposto (Aqui também fizemos a redução de dimensionalidade da categoria `native-country`, a qual não havia sido feito em `model_analysis.ipynb`).
- 📄 `custom_module.py`: arquivo com funções utilizadas para fazer a pipeline final ficar mais legível.

- 📈 `avaliacao-cientista-de-dados-bradesco.pptx`: arquivo para apresentação das análises e resultados obtidos nessa avaliação.

## Tarefas

✅ 1. Carregue a base de dados adult.data e adult.test.

✅ 2. Faça uma análise inicial dos dados: quais problemas você encontrou? Como você trataria tais problemas?

**R:**

- `valores faltando`: Alguns atributos como `workclass`, `occupation` e `native-country` tinham valores faltando, demarcados com o caracter `?`. Para resolver isso, fiz uma substituição considerando o valor com maior frequência de cada atributo.

- `redução de cardinalidade`: Alguns atributos categóricos possuem classes com poucos valores. Por exemplo, no atributo `native-country` quase `90%` dos valores são referentes a `United States`, enquanto outras 41 nacionalidades (incluindo os valores `?`) compõem o restante. Nesse cenário, agrupamos as classes dos atributos considerando a similiridade entre elas quando comparadas à `variável alvo`, isto é, o atributo `class`.

- `atributo "class" (target) desbalanceado`: vimos que nossa variável alvo está desbalanceada no dado de treino (`data/adult.data`), tendo aproximadamente `75%` dos valores na classe `<=50K` e os outros `25%` na classe `>50K`. Para lidar com esse problema, aplicamos a técnica de `oversampling` usando o `SMOTE`.

✅ 3. Faça uma análise exploratória dos dados: quais informações interessantes você encontrou (exemplos: distribuições de variáveis, outliers, correlações, etc.)

**R:**

Considerando as distribuições dos atributos, vimos que algumas classes possuem poucos valores quando comparadas com outras. Por exemplo, no atributo `native-country` quase `90%` dos valores são referentes a `United States`, enquanto outras 41 nacionalidades (incluindo os valores `?`) compõem o resto.

Fazendo uma análise as estatísticas básicas dos atributos numéricos, observamos que eles apresentam diversos `outliers`. Por exemplo, no atributo `fnlwgt` existe uma grande diferença entre o percentil 75% e seu valor máximo. Já para os atributos `capital-gain` e `capital-loss` os valores até o percentil 75% são zero.

Para os atributos numéricos, observamos que as correlações são pequenas. Contudo, quando consideramos a variável alvo (depois de um mapeamento, considerando `<=50K` = 0 e `>50K`= 1), valores razoáveis de correlação foram idenficados, sendo os top três:

- `education-num versus income`: 0,34;
- `age versus income`: 0,23;
- `hours-per-week versus income`: 0,23.

✅ 4. Note que a base de dados é desbalanceada para a variável resposta "class". Utilize uma técnica para balancear os dados, justificando sua escolha.

**R:**

Assim como já foi mencionado, vimos que nossa variável alvo está desbalanceada no dado de treino (`data/adult.data`), tendo aproximadamente `75%` dos valores na classe `<=50K` e os outros `25%` na classe `>50K`. Para lidar com esse problema, aplicamos a técnica de `oversampling` usando o `SMOTE`.

# Modelo e resultados

Após fazer uma análise de alguns modelos (`model_analysis.ipynb`) escolhemos o `XGBoostClassifier`, por apresentar uma uma melhor proporção entre `precision` e `recall`, além de fornecer um bom valor de `accuracy` = 87%:

- classe: `<=50K`:

  - `precision`: 89%
  - `recall`: 91%
  - `F1-score`: 91%

- classe: `>50K`:
  - `precision`: 76%
  - `recall`: 63%
  - `F1-score`: 69%

Segue a matrix de confusão do resultado final:

![confusion_matrix](./figures/confusion_matrix.png)
